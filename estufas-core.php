<?php
/*
Plugin Name: lasmejoresestufas.com CORE
Plugin URI: https://lasmejoresestufas.com
Description: Plugin del core de lasmejoresestufas.com
Version: 1.0
Author: Estupenda
Author URI: https://estupenda.com
License: GPLv2 or later
Text Domain: emc-core
*/

add_filter('upload_mimes', 'estufas_soporte_mimes');
function estufas_soporte_mimes($mime_types = array()){

	$mime_types['svg'] = 'image/svg+xml';
	$mime_types['zip'] = 'application/zip';
	return $mime_types;

}

add_action( 'wp_head', 'estufas_my_google_opt_out_head' );
function estufas_my_google_opt_out_head() {
	if ( is_allowed_cookie( '_ga' ) ) {
		?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KR97RW8');</script>
        <!-- End Google Tag Manager -->
		<?php
	}
}

add_action( 'cancer_body_begin', 'estufas_my_google_opt_out_body' );
function estufas_my_google_opt_out_body() {
	if ( is_allowed_cookie( '_ga' ) ) {
		?>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KR97RW8"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
		<?php
	}
}